# Репозиторий курса "Программирование на языке Python".

### Полезные ссылки

* [telegram-чатик курса](https://t.me/mmf_python_2023)
 * [табличка для MR](https://docs.google.com/spreadsheets/d/1nSmS8Qiral-dTbf2PzIOsfjv-DEpiWmp9Tw0uhHDZ-A/edit?usp=sharing)

### Преподаватели:
Кравчук Артем Витальевич, @akravchuk97 (tg)

Кальмуцкий Кирилл Олегович, @Kirill_Kalmutskiy (tg)

Солдатов Денис Нурхагаевич, @dsoldatov (tg)

### Карта курса

* [week-01](week-01): организационные вопросы, введение в git
* [week-02](week-02): Python, типы и структуры данных, функции
* [week-03](week-03): декораторы
* [week-04](week-04): Структуры данных, модуль collections
* [week-05](week-05): разговоры за кодстайл, os, argparse
* [week-06](week-06): ООП

* [week-08](week-08): менеджеры контекста
* [week-09](week-09): итераторы, генераторы
* [week-10](week-10): аннотации типов, датаклассы

