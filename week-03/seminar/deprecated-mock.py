import time
from functools import wraps
from warnings import warn


def deprecated(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        warn('ne nado menya trogat')
        return func(*args, **kwargs)

    return wrapper


def deprecated_v2(message):
    def deprecated(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            warn(message)
            return func(*args, **kwargs)

        return wrapper

    return deprecated


def mock(return_value):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return return_value

        return wrapper

    return decorator


@deprecated_v2('lolasdoasdasdasd')
def f(x):
    return x


@deprecated_v2('lkll;k;kl;lklk;')
def g(x):
    return x ** 2


@mock(return_value={'1': '2'})
def e(x):
    time.sleep(60)
    print(x)


def func(x: int, y: list):
    print(x, y)

if __name__ == '__main__':
    print(func.__annotations__)
    print(isinstance(1, int))
