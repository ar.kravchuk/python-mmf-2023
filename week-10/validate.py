from pydantic import BaseModel, validator


class User(BaseModel):
    login: str
    password: str
    age: int
    name: str
    surname: str

    @validator('age')
    def validate_age(cls, age):
        if age < 0 or age > 200:
            raise ValueError(f'bad value for age: {age}')
        return age

    @validator('name')
    def capitalize_name(cls, name):
        return name.title()


if __name__ == '__main__':
    user = User(
        login='1',
        password='2',
        age=10,
        name='aRteM',
        surname='4',
    )

    dct = user.dict()

    user1 = User(**dct)
    print(user1)
