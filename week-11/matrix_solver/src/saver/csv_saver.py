import os

import pandas as pd

from src.saver.result_saver import ResultSaver


class CSVSaver(ResultSaver):

    def save(self, result_matrix, result_path):
        os.makedirs(os.path.dirname(result_path), exist_ok=True)
        pd.Series(data=result_matrix, name='x').to_csv(result_path, index=False)
