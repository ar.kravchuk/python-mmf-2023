import abc


class ResultSaver(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def save(self, result_matrix, result_path):
        pass
