import abc


class IOReader(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def read(self, file_path):
        pass
