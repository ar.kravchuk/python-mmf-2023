import pandas as pd

from src.reader.io_reader import IOReader


class CSVReader(IOReader):

    def read(self, file_path):
        df = pd.read_csv(file_path)
        b = df['b'].to_numpy()
        del df['b']
        A = df.to_numpy()
        return A, b


