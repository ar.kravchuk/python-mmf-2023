import numpy as np

from src.solver.matrix_solver import MatrixSolver


class NumpySolver(MatrixSolver):

    def solve(self, A, b):
        return np.linalg.solve(A, b)
