import abc


class MatrixSolver(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def solve(self, A, b):
        pass
