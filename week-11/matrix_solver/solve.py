import argparse

from src.reader import CSVReader
from src.saver import CSVSaver
from src.solver import NumpySolver


class MatrixSolutionFinder:

    def __init__(self):
        self._reader = CSVReader()
        self._saver = CSVSaver()
        self._solver = NumpySolver()

    def run(self, matrix_path, result_path):
        A, b = self._reader.read(file_path=matrix_path)
        x = self._solver.solve(A=A, b=b)
        self._saver.save(result_path=result_path,result_matrix=x)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--matrix_path', type=str, required=True)
    parser.add_argument('--result_path', type=str, required=True)
    args = parser.parse_args()

    matrix_solution_finder = MatrixSolutionFinder()
    matrix_solution_finder.run(
        matrix_path=args.matrix_path,
        result_path=args.result_path
    )
