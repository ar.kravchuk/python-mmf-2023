# Домашнее задание

# Правила игры

### Когда сдаем, куда сдаем

* #### Дедлайн: 30 апреля, 23-50

* Записываем МР'ы в [эту](https://docs.google.com/spreadsheets/d/1nSmS8Qiral-dTbf2PzIOsfjv-DEpiWmp9Tw0uhHDZ-A/edit?usp=sharing) 
табличку (лист main-hw-01)

* После ревью дается еще неделя (от даты ревью) на исправление замечаний.



### Еще раз про списывание

* За списывание баллы будут обнуляться всем участникам. Нам не важно, кто главный автор
* Можно помогать друг другу со всем, что связано с git'ом, прекоммитом и настройкой линтеров. Это не считается списыванием

---


## 1. Check ignored (20б.)
Напишите программу, которой на вход аргументами командной строки передаются путь некоторого репозитория, 
который содержит файл `.gitignore`. 

Считаем, что в `.gitignore` могут находиться только следующие конструкции:

* Конкретные пути до файлов: `path/to/some/file.bin`
* Регулярные выражения, начинающиеся со \* : `*.txt`, `*trash.csv`

### Пример: 

Для такой файловой структуры:

    some_project
       ├── first_folder
       │   ├── first_subfolder
       │   │   ├── file1.dbf
       │   │   ├── file2.shp
       │   │   ├── file3.shx
       │   │   └── file4.pickle
       │   └── file5.csv
       ├── second_folder
       │   ├── sedond_subfolder
       │   │   ├── file1.dbf
       │   │   ├── file10.py
       │   │   ├── not_file.bin
       │   │   └── another_file.txt
       │   └── yet_another_file.yml
       └── .gitignore 
       ├── coverage.html
       ├── run_script.py

И такого содержания файла `.gitignore`:

```
first_folder/first_subfolder/file2.shp
coverage.html
*.dbf
second_folder/useless.py
```

После запуска скрипта из консоли:

``python3 check_ignored.py --project_dir=/path/to/some_project``

Вывод должен быть:

```
Ignored files:
some_project/first_folder/first_subfolder/file2.shp ignored by expression first_folder/first_subfolder/file2.shp
coverage.html ignored by expression coverage.html
some_project/first_folder/first_subfolder/file1.dbf ignored by expression *.dbf
some_project/second_folder/second_subfolder/file1.dbf ignored by expression *.dbf

```

## Что погуглить
* `os.listdir`
* `os.walk`
* `python argparse`
* `python re`
* `re.match`

# 2. "Умная" корзина (30 б.)

Пусть мы устали каждый раз руками чистить корзину, поэтому решили написать скрипт, который это будет делать за нас.

Сразу удалять файлы из корзины не хочется, потому что мы могли добавить добавить их в корзину случайно. Мы хотим, чтобы из корзины удалялись только те файлы, чей возраст превышает `age_thr`.

В этой задаче считаем, что корзина - это просто какая-то папка, в которую мы можем перекидывать другие файлы и папки

Те же требования, только более формально:

Напишите скрипт, который

* Удаляет из папки те файлы, чей возраст превышает `age_thr`
* Удаляет пустые папки из корзины  
* "Засыпает" на секунду, а затем опять возвращается проверить, не устарели ли какие-нибудь файлы
* Записывает в файлик `clean_trash.log` пути до тех файлов и папок, которые были удалены.

Скрипт должен запускаться из консоли со следующими параметрами:

``python3 clean_trash.py --trash_folder_path=path/to/your/trash --age_thr=120``

* `trash_folder_path`: путь до папки, из которой будем удалять файлики
* `age_thr`: время "устаревания" файла/папки в секундах, после которого нужно удалять

## Что погуглить

* `os.path.getmtime`
* `os.walk`
* `python argparse` 
* `os.remove` 
* `os.rmdir` 
* `time.sleep` 
* `python logging` (это опционально, можно просто  в файл писать, но если хочется по красоте, то лучше через него)
